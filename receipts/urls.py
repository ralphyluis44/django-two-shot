from django.urls import path, include
from receipts.views import receipts_list, create_receipt_view, create_category, create_account, category_list, account_list

urlpatterns = [
    path("", receipts_list, name="home"),
    path('accounts/', include('accounts.urls')),
    path('create/', create_receipt_view, name='create_receipt'),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),

]